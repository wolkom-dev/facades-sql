import { DatabaseStorageFacade, DatabaseStorageFacadeOptions } from '../types';
import { Database , Pool } from 'odbc';
import bunyan = require('bunyan')
import { log }  from '../log';
import { WriteOpsResults } from '../types/CollectionFacade';

export class SQLDatabaseStorage implements DatabaseStorageFacade<Database> {
	protected _db: Database | null
	protected _log: bunyan
	protected _loglevel: DatabaseStorageFacadeOptions['logLevel']
	protected _connectionUrl: string | undefined

	constructor (options?: DatabaseStorageFacadeOptions) {
		this._db = null
		this._connectionUrl = (options && options.connectionUrl) ? options.connectionUrl : undefined
		this._loglevel = options ? (options.logLevel ? options.logLevel : 'info') : 'info'
		this._log = log
		this._log.level(this._loglevel)
	}
	/**Connect
	 *
	 * Creates a connection to the database
	 *
	 * @example
	 * facade.connect().then((database) => {database.query()}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<Database>
	 */
	connect(connectionUrl?: string): Promise<Database> {
		return new Promise(async (resolve, reject) => {
			const connection: string | undefined = connectionUrl || this._connectionUrl
			if (!connection) {
				this._log.error(`ConnectionUrl not defined`)
				reject(new Error(`ConnectionUrl not defined`))
			} else if (this._db) {
				resolve(this._db)
			} else {
				await new Pool().open(connection, async (err: Error, database: Database) => {
					if (err) {this._log.error(new Date(), ' : ', err); reject(err)}
					this._log.info(new Date(), ' : database connected')
					this._db = await database
					resolve(database)
				})
			}
		})

	}
	/**closeConnection
	 *
	 * Closes connection to the database
	 *
	 * @example
	 * facade.closeConnection().then(() => {console.log('connection closed')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<void>
	 */
	closeConnection(): Promise<void> {
		return new Promise((resolve, reject) => {
			if (this._db) {
				this._db.close((err: Error) => {
					if (err) {this._log.error(new Date(), ' : ', err); reject(err)} else
					this._log.info(new Date() , ' : database connection closed')
					resolve()
				})
			} else {this._log.warn(new Date(), ' : database connection closed - no connection to close'); resolve()}
		})
	}
	/**custom query
	 *
	 * allows for custom SQL string expression for complex queries
	 *
	 * @param query			:custom query string
	 * @param params		:optional query parameters for query
	 *
	 * @example
	 * facade.query('SELECT * FROM TABLE WHERE i = 1).then((data) => {console.log(data)}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<ResultRow[]>
	 */
	query(query: string, params?: string[]|number[]|boolean[]): Promise<WriteOpsResults<{}>> {
		return new Promise((resolve, reject) => {
			if (this._db) {
				const statement = this._db.prepareSync(query)
				statement.execute(params ? params : [], (err1: Error, result) => {
					if (err1) {this._log.error(new Date(), ' : ', err1); reject(err1)}
					result.fetchAll((err2: Error, data) => {
						if (err2) {this._log.error(err2); reject(err2)}
						this._log.info(new Date(), ' : ', data);
						resolve({stats: {error: false, created: 0, errors: 0, empty: 0, updated: 0, removed: 0, ignored: 0, code: 200}, data: data});
					})
				})
			} else {this._log.error(new Date(), ' : Database not connected'); reject('Database not connected')}
		})
	}
}

export default SQLDatabaseStorage