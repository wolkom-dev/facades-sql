import { Modelschema } from '@simplus/si-simple-schema';
import { SQLDialects } from 'sql';

export interface DatabaseStorageFacadeOptions {
	/**Connection Url
     *
     * Consider the following formats:
     * ODBC => DRIVER={FreeTDS};SERVER=host;UID=user;PWD=password;DATABASE=dbname
     */
	connectionUrl: string
	/**Log level
     *
     * Set the minimum level at which you want logging to occur.
     */
	logLevel?: 'fatal' | 'error' | 'warn' | 'info' | 'debug' | 'trace'
}


export interface CollectionFacadeOptions extends DatabaseStorageFacadeOptions {
	/**ModelSchema
     *
     * Definition of storage rule for validation.
     *
    */
	schema: Modelschema
	/**Collection name
     *
     *Name of collection storage
     */
	collection: string
	validation?: boolean

	schemaname?: string

	dialect?: SQLDialects
}
export default CollectionFacadeOptions