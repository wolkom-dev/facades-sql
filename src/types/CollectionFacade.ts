import { DatabaseStorageFacade } from './DatabaseStorageFacade'
import { Filter, Update} from '@simplus/si-query-object'
import { Database } from 'odbc';


export interface WriteOpsResults<Model> {
	stats: WriteOpsStats
	data: Model[]
}
export interface WriteOpsResult<Model> {
	stats: WriteOpsStats
	data: Model
}

export interface WriteOpsStats {
	error: boolean
	created?: number
	errors?: number
	empty?: number
	updated?: number
	removed?: number
	ignored?: number
	code?: number
}

/**
 * Collection Interface.
 *
 * Interface for generic collection methods on all databases.
 */
export interface CollectionFacade<Model> extends DatabaseStorageFacade<Database> {
	/**Create
	 *
	 * inserts data into database
	 *
	 * @param model		:model object to insert as array
	 *
	 * @example
	 * facade.create({name: Athenkosi, surname: Mase}).then((data) => {console.log('model inserted')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<Model[]>
	 */
	create(...model: Model[]): Promise<WriteOpsResults<Model>> // breaking-change
	/**DuplicateById
	 *
	 * creates a duplicate entry with a new unique id
	 *
	 * @param id		:unique id of element to duplicate
	 *
	 * @example
	 * facade.duplicateById('12345').then((data) => {console.log('model duplicated')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<Model>
	 */
	duplicateById(id: string): Promise<WriteOpsResult<Model>>
	/**Update
	 *
	 * update element(s) with new model information based on filter.
	 *
	 * @param filter		:filter parameters to segment items
	 * @param model			:rule to update model
	 *
	 * @example
	 * facade.update({name: {$eq : 'Athenkosi'}}, {$set :{name: 'Yehudi'}}).then((data) => {console.log('name updated')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<UpdateWriteOpResult>
	 */
	update(filter: Filter, model: Update): Promise<WriteOpsResults<Model>>
	/**Update By Id
	 *
	 * update element with new model information based on id.
	 *
	 * @param id			:element unique id
	 * @param model			:rule to update model
	 *
	 * @example
	 * facade.updateById('12345', {$set :{name: 'Yehudi'}}).then((data) => {console.log('name updated')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<UpdateWriteOpResult>
	 */
	updateById(id: string, model: Update): Promise<WriteOpsResult<Model>>
	/**Find One
	 *
	 * find first element to appear based on filter
	 *
	 * @param filter		:filter parameters to segment items
	 *
	 * @example
	 * facade.findOne({name: {$eq : 'Athenkosi'}}).then((data) => {console.log(data)}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<ResultRow>
	 */
	findOne(filter: Filter): Promise<Model | null>
	/**Find By Id
	 *
	 * find element based on the unique id
	 *
	 * @param id		:unique identifier string
	 *
	 * @example
	 * facade.findById('1234').then((data) => {console.log(data)}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<ResultRow>
	 */
	findById(id: string): Promise<Model | null>
	/**Find any
	 *
	 * find any or all elements in collection
	 *
	 * @param filter		:filter parameters to segment items
	 *
	 * @example
	 * facade.findById({month: {$eq: 'January'}}).then((data) => {console.log(data)}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<ResultRow[]>
	 */
	find(filter: Filter): Promise<Model[] | null>
	/**Remove any
	 *
	 * delete any or all elements in collection
	 *
	 * @param filter		:filter parameters to segment items
	 *
	 * @example
	 * facade.remove({month: {$eq: 'January'}}).then((res) => {console.log('items removed')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<DeleteWriteOpResultObject>
	 */
	remove(filter: Filter): Promise<WriteOpsStats>
	/**Remove by id
	 *
	 * delete element based on the unique identifier
	 *
	 * @param id		:unique identifier string
	 *
	 * @example
	 * facade.removeById('1234').then((res) => {console.log('item removed')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<DeleteWriteOpResultObject>
	 */
	removeById(id: string): Promise<WriteOpsStats>
	/**createUniqueIndex
	 *
	 * Creates a index on a column
	 *
	 * @param column	:index column
	 * @param indexname	:optional name for index
	 *
	 * @example
	 * facade.closeConnection().then(() => {console.log('connection closed')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<void>
	 */
	createUniqueIndex (...column: string[]): void
}


//////////////////////////////////////////////////////////////////////////////////////////////////
// Export CRUD Facade
//////////////////////////////////////////////////////////////////////////////////////////////////
export default CollectionFacade;
