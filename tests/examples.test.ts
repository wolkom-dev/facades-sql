import { SQLCollection, CollectionFacadeOptions } from '../src';
import { SQLDialects } from 'sql';
import { log } from '../src/log';
import { Database } from 'odbc';
import chai = require('chai');

interface Model {
	_id?: string,
	email: string,
	password: string,
	email_verified: boolean,
	name: string,
	active: boolean
}

let facade: SQLCollection<Model>
let db: Database
let testid: string
const options: CollectionFacadeOptions = {
	schema: {
		_id: {
			dataType : String
		},
		email : {
			dataType : String,
			name : 'email'
		},
		password : {
			dataType : String,
			name : 'password'
		},
		email_verified : {
			dataType : Boolean,
			name : 'email_verified'
		},
		name : {
			dataType : String,
			name : 'name'
		},
		active : {
			dataType : Boolean,
			name : 'active'
		}
	},
	collection: 'Examples',
	connectionUrl: String(process.env.CONNECTION_STRING),
	dialect: process.env.DIALECT as SQLDialects,
	logLevel: 'warn',
	schemaname: process.env.SCHEMA || undefined,
	validation: true
}

describe('SQL collection usage examples', function(): void {
	before( function(done: MochaDone): void {
		// tslint:disable-next-line:no-invalid-this
		this.timeout(3000)
		facade = new SQLCollection(options)
		facade.connect()
			.then((database) => {
				db = database; log.info('Connected'); done()})
			.catch((err) => {done(err)})
	})

	after(function(done: MochaDone): void {
		facade.dropCollection()
		.then(() => {
			log.info('Collection dropped')
			facade.closeConnection()
				.then(() => {log.info('connection closed'); done()})
				.catch((err) => {done(err)})
		})
		.catch(done)
	})

	describe('Create test case', function(): void {
		it('should create a single test case', function(done: MochaDone): void {
			facade.create({email: 'Yehudi@simplus.com', password: '1234', email_verified: true, name: 'Yehudi', active: true}, { email: 'Athi@simplus.com', password: '54321', email_verified: true, name: 'Athi', active: true})
				.then((model) => {
					chai.expect(model.data[0]._id).not.to.equal(undefined)
					testid = model.data[0]._id as string
					done()
				}).catch(done)
		})
	})

	describe('Find test case', function(): void {
		it('should find test case', function(done: MochaDone): void {
			facade.find({})
				.then((models) => {
					chai.expect(models).to.be.an('array')
					done()
				}).catch(done)
		})
		it('should find test case', function(done: MochaDone): void {
			facade.findById(testid)
				.then((model) => {
					chai.expect(model._id).not.to.equal(undefined)
					chai.expect(model.active).to.equal('true')
					done()
				}).catch(done)
		})
		it('should replace the object updated by id', function(done: MochaDone): void {
			facade.updateById(testid, {email: 'Yehudi@simplus.com', password: 'password', email_verified: true, name: 'Yehudi', active: true})
				.then((model) => {
					chai.expect(model).to.be.an('object')
					done()
				}).catch(done)
		})
	})
})